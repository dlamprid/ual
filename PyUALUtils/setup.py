#!/usr/bin/env python

from distutils.core import setup

setup(name='PyUALUtils',
      version='1.0',
      description='Python utilities for UAL library',
      author='Federico Vaga',
      author_email='federico.vaga@cern.ch',
      maintainer="Federico Vaga",
      maintainer_email="federico.vaga@cern.ch",
      url='https://gitlab.cern.ch/cohtdrivers/ual',
      packages=['PyUALUtils'],
      license='LGPLv3',
     )

